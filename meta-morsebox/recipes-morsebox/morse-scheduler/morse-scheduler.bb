SUMMARY = "Morse sheduler"
DESCRIPTION = "Sends a morse message at a specific time and interval"

LICENSE = "CLOSED"

RDEPENDS:${PN} += "morse-sender"

SRC_URI  = "file://morse-schedule.cron"
SRC_URI += "file://scheduled-msg"

S = "${WORKDIR}"

do_install() {
    install -d ${D}${sysconfdir}/morsebox
    install -m 0755 scheduled-msg ${D}${sysconfdir}/morsebox/

    install -d ${D}${sysconfdir}/cron.d
    install -m 0600 morse-schedule.cron ${D}${sysconfdir}/cron.d/
}
