require recipes-core/images/core-image-base.bb

EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

inherit extrausers

EXTRA_USERS_PARAMS = " useradd gpio; \
 groupadd gpio; \
 usermod -a -G gpio daemon;"

# Include packages
IMAGE_INSTALL += "dnsmasq"
IMAGE_INSTALL += "hostapd"
IMAGE_INSTALL += "cronie"
IMAGE_INSTALL += "rpi-gpio"
IMAGE_INSTALL += "python3"
IMAGE_INSTALL += "rfs-morsebox"
IMAGE_INSTALL += "morse-sender"
IMAGE_INSTALL += "morse-scheduler"
IMAGE_INSTALL += "rtc-pi"

#IMAGE_INSTALL += "apache2"
#IMAGE_INSTALL += "php"
#IMAGE_INSTALL += "php-cli"
#IMAGE_INSTALL += "php-modphp"
#IMAGE_INSTALL += "webapp"

