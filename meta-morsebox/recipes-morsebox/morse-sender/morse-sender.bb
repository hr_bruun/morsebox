SUMMARY = "Morse Sender Program"
DESCRIPTION = "Sends a morse message to a GPIO pin"

LICENSE = "CLOSED"

RDEPENDS:${PN} += "python3"

SRC_URI = "file://morse-sender.py"

S = "${WORKDIR}"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 morse-sender.py ${D}${bindir}
}
