#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A program that can send a morse message to a connected LED.

The LED is expected to be connected to GPIO 4 (BCM naming)

The message to send is specified with the -m parameter

'''

import sys
import argparse
import time

TEST_MODE = False
try:
    import RPi.GPIO as GPIO
except ImportError:
    TEST_MODE = True

alphabet = {
    "a": [ 0, 1 ],
    "b": [ 1, 0, 0, 0 ],
    "c": [ 1, 0, 1, 0 ],
    "d": [ 1, 0, 0 ],
    "e": [ 0 ],
    "f": [ 0, 0, 1, 0 ],
    "g": [ 1, 1, 0 ],
    "h": [ 0, 0, 0, 0 ],
    "i": [ 0, 0 ],
    "j": [ 0, 1, 1, 1 ],
    "k": [ 1, 0, 1 ],
    "l": [ 0, 1, 0, 0 ],
    "m": [ 1, 1 ],
    "n": [ 1, 0 ],
    "o": [ 1, 1, 1 ],
    "p": [ 0, 1, 1, 0 ],
    "q": [ 1, 1, 0, 1 ],
    "r": [ 0, 1, 0 ],
    "s": [ 0, 0, 0 ],
    "t": [ 1 ],
    "u": [ 0, 0, 1 ],
    "v": [ 0, 0, 0, 1 ],
    "w": [ 0, 1, 1 ],
    "x": [ 1, 0, 0, 1 ],
    "y": [ 1, 0, 1, 1 ],
    "z": [ 1, 1, 0, 0 ],
    "æ": [ 0, 1, 0, 1 ],
    "ø": [ 1, 1, 1, 0 ],
    "å": [ 0, 1, 1, 0, 1 ]
}

def dit_length(wpm):
    # Using the "PARIS" standard word 
    return 60 / (50 * wpm)


GPIO_PIN = 4

def setup_gpio():
    if not TEST_MODE:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIO_PIN, GPIO.OUT)
        GPIO.output(GPIO_PIN, GPIO.LOW)
    
def cleanup_gpio():
    if not TEST_MODE:
        GPIO.output(GPIO_PIN, GPIO.LOW)
        GPIO.cleanup()

def transmit(signal):
    GPIO.output(GPIO_PIN, GPIO.HIGH)
    if signal == 0:
        pause(1)
    else:
        pause(3)

    GPIO.output(GPIO_PIN, GPIO.LOW)
    pause(1)

def debug(msg):
    print(msg, end=' ', flush=True)

def test_transmit(signal):
    if signal == 0:
        debug("dot")
        pause(1)
    else:
        debug("dash")
        pause(3)
    
def pause(units):
    time.sleep(units * DIT_LENGTH)

def send_code(letter):
    if letter == " ":
        # End of word so we add an extra /
        debug("/")
        print(" - pause - ")
        pause(7)
    else:
        for dot_or_dash in alphabet[letter]:
            if TEST_MODE:
                test_transmit(dot_or_dash)
            else:
                transmit(dot_or_dash)
            pause(1) # intra-character space
                
        if TEST_MODE:
            debug("/")

        pause(2) # Makes 3 in all for inter-character space

# -- Main program -- #

def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-m', help='the message to send')
    group.add_argument('-f', help='a file with the message to send')
    parser.add_argument('--wpm', nargs='?', const=2, type=int, default=2,
                        help='the speed of the message in words per minute (default is 1)')

    if not len(sys.argv) > 1:
        print(parser.print_help())
        exit()

    args = parser.parse_args()

    global DIT_LENGTH
    DIT_LENGTH = dit_length(args.wpm)
    print(f'Words per minute: {args.wpm}')
    print(f'Dit speed in seconds: {DIT_LENGTH}')

    if args.f:
        with open(args.f, 'r') as file:
            message = file.read().replace('\n', '')

    if args.m:
        message = args.m

    print(f'Sending message: {message}')

    setup_gpio()

    for letter in message.lower():
        send_code( letter )

    cleanup_gpio()

if __name__ == "__main__":
    main()
