FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://interfaces"

do_install:append() {
	install -d ${D}${sysconfdir}/network
	install -m 0755 ${WORKDIR}/interfaces ${D}${sysconfdir}/network/
}

