SUMMARY = "MyProject Additional files"
#PROVIDES = "rfs-morsebox"
LICENSE = "CLOSED"

SRC_URI = "file://99-morsebox.rules"

do_install[nostamp] = "1"
do_unpack[nostamp] = "1"

do_install () {
    echo "my-project install task invoked"
    install -d ${D}${sysconfdir}/udev/rules.d/
    install -m 0666 ${WORKDIR}/99-morsebox.rules ${D}/etc/udev/rules.d/99-morsebox.rules
}

FILES:${PN} += " /etc/udev/rules.d/99-morsebox.rules"

PACKAGES = "${PN}"

