SUMMARY = "RTC Program for ds1302"
DESCRIPTION = "See more: https://playground.arduino.cc/Main/DS1302/"
HOMEPAGE = "https://zedt.eu/tech/hardware/using-ds1302-real-time-clock-module-raspberry-pi/"

LICENSE = "CLOSED"

SRC_URI = "file://rtc-pi.c \
           file://rtc.py \
	   file://settimefromrtc \
"

S = "${WORKDIR}"

do_compile() {
         ${CC} rtc-pi.c -o rtc-pi
}

INSANE_SKIP:${PN} = "ldflags"

do_install() {
         install -d ${D}${bindir}
         install -m 0755 rtc-pi ${D}${bindir}
         install -m 0755 rtc.py ${D}${bindir}

	 install -d ${D}${sysconfdir}/init.d/
	 install -m 0755 settimefromrtc ${D}${sysconfdir}/init.d/

	 install -d ${D}${sysconfdir}/rc5.d/
	 cd ${D}${sysconfdir}/rc5.d/
	 ln -s ../init.d/settimefromrtc S01settimefromrtc
}
