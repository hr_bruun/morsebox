SUMMARY = "Morsebox Web Application"
DESCRIPTION = "Recipe created by bitbake-layers"
LICENSE = "CLOSED"

SRC_URI = "file://webapp.php \
           file://index.html \
           file://morse.php \
           file://keyboard.css \
           file://keyboard.js \
           file://jquery.min.js \
           "

FILES:${PN} += "${datadir}/apache2/default-site/htdocs/*"

ALLOW_EMPTY:${PN} = "1"

python do_build() {
    bb.plain("***********************************************");
    bb.plain("*                                             *");
    bb.plain("*  WebApp recipe created by bitbake-layers   *");
    bb.plain("*                                             *");
    bb.plain("***********************************************");
}

do_install() {
	install -d ${D}${datadir}/apache2/default-site/htdocs/
	install -m 0755 ${WORKDIR}/index.html ${D}${datadir}/apache2/default-site/htdocs/
	install -m 0755 ${WORKDIR}/*.php ${D}${datadir}/apache2/default-site/htdocs/
	install -m 0755 ${WORKDIR}/*.js ${D}${datadir}/apache2/default-site/htdocs/
	install -m 0755 ${WORKDIR}/*.css ${D}${datadir}/apache2/default-site/htdocs/
}
