<?php
if(isset($_POST['submit'])){
  $message = $_POST['msg'];
  # debug command
  # $cmd = "morse-sender.py -m \"" . $message . "\" 2>&1";
  $cmd = "morse-sender.py -m \"" . $message . "\"";
  echo "<p>$cmd</p>";
  $output = shell_exec($cmd);
  echo "<p>Output: $output</p>";
}
?>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="keyboard.css" />
  </head>

  <body>
    <p>MorseBox Web Application</p>

    <form action="" method="POST">
      <input name="msg" type="text"/>
      <input name="submit" type="submit"/>
    </form>

    <div id="container">
      <textarea id="write" rows="6" cols="60"></textarea>
      <ul id="keyboard">
        <li class="letter row1">q</li>
        <li class="letter">w</li>
        <li class="letter">e</li>
        <li class="letter">r</li>
        <li class="letter">t</li>
        <li class="letter">y</li>
        <li class="letter">u</li>
        <li class="letter">i</li>
        <li class="letter">o</li>
        <li class="letter">p</li>
        <li class="letter row2">a</li>
        <li class="letter">s</li>
        <li class="letter">d</li>
        <li class="letter">f</li>
        <li class="letter">g</li>
        <li class="letter">h</li>
        <li class="letter">j</li>
        <li class="letter">k</li>
        <li class="letter">l</li>
        <li class="letter row3">z</li>
        <li class="letter">x</li>
        <li class="letter">c</li>
        <li class="letter">v</li>
        <li class="letter">b</li>
        <li class="letter">n</li>
        <li class="letter">m</li>
      </ul>
    </div>

    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="keyboard.js"></script>
</body>
</html>
