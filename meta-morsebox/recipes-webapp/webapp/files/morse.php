<?php
  
  if( isset( $_GET['msg'] )) {
    $message = $_GET['msg'];
    echo "Message [" . $message . "] sent";
    # debug command
    # $cmd = "morse-sender.py -m \"" . $message . "\" 2>&1";
    $cmd = "morse-sender.py -m \"" . $message . "\"";
    # echo "<p>$cmd</p>";
    $output = shell_exec($cmd);
    echo "<p>Output: $output</p>";
  } else {
    echo "No message sent";
  }

?>
