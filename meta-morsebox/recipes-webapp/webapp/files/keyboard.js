$(function(){

    $("#msgform").submit(function(e) {

	e.preventDefault(); // avoid to execute the actual submit of the form.

	var form = $(this);
	var url = form.attr('action');
	var message = $("#textmsg").val();
	$.get(url,
	      {
		  msg: message
	      },
	       function (data) {
		   $("#notification").html(data);
	       }
        );
    });
    
    $('#keyboard li').click(function(){
	var $this = $(this),
	    character = $this.html();	
	
	$.get("morse.php",
	       {
		   msg: character
	       },
	       function (data) {
		   $("#notification").html(data);
	       }
	     );

	// Add the character
	//$write.html($write.html() + character);
	return false;
    });
});
