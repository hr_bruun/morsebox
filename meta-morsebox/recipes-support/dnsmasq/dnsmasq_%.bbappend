FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://dnsmasq.conf"

do_install:append() {
	install -d ${D}${sysconfdir}
	install -m 0755 ${WORKDIR}/dnsmasq.conf ${D}${sysconfdir}
}

