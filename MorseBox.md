Improvements:

- Improve HTML page
  - to work with mobile browsers
  - Add keyboard for easy selection of letters
  - Add WPM selection
  - Add repeat option
  - Stop sending

- Add Farnsworth Timing
- Add dns so that the page could be morsebox.dk instead of 192.168.5.1

