#!/usr/bin/env bash

usage() {
  echo
  echo "Usage:"
  echo ". $arg0 <machine>"
  echo
  echo "Please specify one of the following machines:"
  echo "  raspberrypi3" 
  echo
  return 0
}

shell=$(ps -p "$$")
if [ -n "${shell##*zsh*}" ] && [ -n "${shell##*bash*}" ]; then
  echo "Error: We require running Yoe in a bash or zsh shell. Other shells have not been tested."
  return 1
fi

if [ -n "$BASH" ]; then
    arg0=$BASH_SOURCE[0]
elif [ -n "$ZSH_NAME" ]; then
    arg0=$0
else
    arg0="$(pwd)/envsetup.sh"
    if [ ! -e "$arg0" ]; then
        echo "Error: $arg0 doesn't exist!" >&2
        echo "Please source this script in same directory where $arg0 lives" >&2
        exit 1
    fi
fi

if [ -z "$ZSH_NAME" ] && [ "$0" = "$arg0" ]; then
    echo "Error: This script needs to be sourced. Please run as '. $arg0'" >&2
    exit 1
fi

BB_ENV_PASSTHROUGH_ADDITIONS="$BB_ENV_PASSTHROUGH_ADDITIONS DISTRO_VARIANT MACHINE"
export BB_ENV_PASSTHROUGH_ADDITIONS

BASE_DIR=$(readlink -f $(dirname $arg0))
echo "BASE_DIR: $BASE_DIR"

cp $BASE_DIR/conf/local.conf.template $BASE_DIR/build/conf/local.conf
cp $BASE_DIR/conf/bblayers.conf.template $BASE_DIR/build/conf/bblayers.conf
