This README file contains information on the contents of the meta-morsebox layer.

Please see the corresponding sections below for details.

# Dependencies

Currently MorseBox only works with Raspberry Pi 3.

The following tools should be installed before building and flashing
the software.

$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat libsdl1.2-dev xterm bmap-tools

# Check out the morsebox project

git clone --recurse-submodules https://bitbucket.org/hr_bruun/morsebox.git

Source the build environment

```
$ mkdir build
$ cd build
$ source ../poke/oe-init-build-env .
$ source ../envsetup.sh
```

Start the build

$ bitbake morsebox-image

Your Raspberry Pi image will be here

~/morsebox-tmp/deploy/images/raspberrypi3/mmorsebox-image-raspberrypi3.wic.bz2

Use bmaptool to copy the generated .wic.bz2 file to the SD card

**Make sure that /dev/sdb is the right device**

$ sudo bmaptool copy morsebox-image-raspberrypi3.wic.bz2 /dev/sdb


**On MacOS**
$ diskutil list
$ sudo diskutil umountDisk /dev/disk2
$ bzcat UbuntuMacShared/morsebox-image.wic.bz2 | sudo dd of=/dev/disk2


# Usage

## Attaching the LED

The positive side of the LED must be attached to PIN 4 and the negative side to GND.

See the layout of the PINS here: https://www.raspberrypi.org/documentation/usage/gpio/

## Connecting to MorseBox

The Morsebox will create a wifi access point 'morsebox' with the password 'samuelmorse'.

Connect to the access point and load the page http://192.168.5.1/


